# Usa la imagen base de Alpine
FROM alpine:latest

# Actualiza el índice de paquetes e instala las herramientas necesarias
RUN apk update && apk add --no-cache vim bash openssh-server openssh-client neofetch

# Crea y establece permisos para los directorios necesarios
RUN mkdir -p /run/sshd && \
    ssh-keygen -A

# Establece una contraseña para root (cambia 'rootpassword' a la que prefieras)
RUN echo 'root:rootpassword' | chpasswd

# Cambia el shell predeterminado de root a bash
RUN sed -i 's/\/bin\/sh/\/bin\/bash/' /etc/passwd

# Configura SSHD para permitir el acceso por contraseña y root
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/' /etc/ssh/sshd_config

# Creo .bashrc y agrego neofetch
RUN echo "neofetch" > /root/.bashrc
RUN echo 'export PS1="\[\e[32m\]\A \[\e[34m\]\u\[\e[0m\]@\[\e[35m\]\h \[\e[36m\]\w\[\e[0m\] "' >> /root/.bashrc
RUN echo "/usr/sbin/sshd" >> /root/.bashrc

# Exponer el puerto 22 para SSH
EXPOSE 22

# Ejecutar el servidor SSH al iniciar el contenedor
CMD ["bash"]
